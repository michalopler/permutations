#include "genincreasing.hpp"
#include "gen1324.hpp"
#include "genany.hpp"
#include <iostream>
#include <chrono>

using namespace std;

int main()
{
    typedef std::chrono::high_resolution_clock clock;

    #ifdef DEBUG
        uint size = 500;
    #else
        uint size = 10000;
    #endif

    Generator g({1,3,2,4}, 100 );
    Generator1324 g2(100);

    // test for same sequences
    clock::duration d = clock::now().time_since_epoch();
    g.setSeed(d.count());
    g2.setSeed(d.count());


    auto tb = std::chrono::steady_clock::now();
    g2.move(size);
    auto te = std::chrono::steady_clock::now();
    double t1 = std::chrono::duration_cast<std::chrono::microseconds>( te - tb).count() / (double) size;

    tb = std::chrono::steady_clock::now();
    g.move(size);
    te = std::chrono::steady_clock::now();
    double t2 = std::chrono::duration_cast<std::chrono::microseconds>( te - tb).count() / (double) size;

    cout << "Average time spent on 1 step (1324): " << t1 << "microseconds" << endl;
    cout << "Average time spent on 1 step (Generic): " << t2 << "microseconds" << endl;
    cout << "Number of successful moves (1324) " << g2.getMoveCount() << "/" << size << endl;
    cout << "Number of successful moves (Generic) " << g.getMoveCount() << "/" << size << endl;

    savePlotData(g.getPerm(), "perm.dat");
    savePlotData(g2.getPerm(), "perm2.dat");
    return 0;
}
