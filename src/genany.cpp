#include <cmath>
#include <algorithm>
#include <deque>
#include <tuple>
#include <assert.h>
#include "genany.hpp"

using namespace std;


Generator::Generator(const perm &p, uint n):
    pattern(normalizePerm(p)), actPerm(n),
    impElms(4*p.size(),-1), offsets(4*p.size(),-1), unfrgtElms(pattern.size() + 1),
    quadrants(4*p.size(),false), markovPos(0), moveCount(0), permSize(n),
    patternSize(pattern.size()),
    rd(), re(rd()), rg(0, n-1)
{
    // set exporder
    findExpOrder();

    // calculate data
    calcImpElms();

    // calculate quadrants
    calcQuadrants();

    // set decreasing permutation as default beginning
    for(uint i = 0; i < permSize; i++)
            actPerm[i] = permSize - i - 1;


    // if the pattern is strictly decreasing, then set increasing
    // permutation as beginning
    if(test())
    {
        for(uint i = 0; i < permSize; i++)
             actPerm[i] = i;
    }
}

Generator::~Generator()
{
}


// moves given number of steps in Markov chain
void Generator::move(uint steps)
{
    for(uint n = 0; n < steps; ++n)
    {
        markovPos++;

        // get two random positions
        ushort a = rg(re);
        ushort b = rg(re);

        // other than two equal
        if(a == b) continue;

        moveElm(a,b);

        // rollback when not succesful
        if(test(b, actPerm[b]))
        {
            moveElm(b,a);
        }
        else moveCount++;
    }
}



// precalculates data for given exposure order
void Generator::calcImpElms()
{
    // values for first step - no limits
    impElms[0] = -1;
    impElms[1] = -1;
    impElms[2] = -1;
    impElms[3] = -1;

    // and offsets
    offsets[0] = exporder[0];
    offsets[1] = patternSize - 1 - exporder[0];
    offsets[2] = pattern[exporder[0]];
    offsets[3] = patternSize - 1 - pattern[exporder[0]];

    // calculate important elements for each step
    for(uint i = 1; i < patternSize; i++)
    {
        int pmin = -1;
        int pmax = -1;
        int qmin = -1;
        int qmax = -1;

        int value = pattern[exporder[i]];

        // find min and max
        for(uint j = 0; j < i; j++)
        {
            if(exporder[j] < exporder[i] && exporder[j] > pmin)
                pmin = exporder[j];
            else if(exporder[j] > exporder[i] && (pmax == -1 || exporder[j] < pmax))
                pmax = exporder[j];

            if(pattern[exporder[j]] < value && (qmin == -1 || pattern[exporder[j]] > pattern[qmin]))
                qmin = exporder[j];
            else if(pattern[exporder[j]] > value && (qmax == -1 || pattern[exporder[j]] < pattern[qmax]))
                qmax = exporder[j];
        }

        // set important elements
        impElms[4*i+0] = pmin;
        impElms[4*i+1] = pmax;
        impElms[4*i+2] = qmin;
        impElms[4*i+3] = qmax;

        // and their offsets
        offsets[4*i+0] = pmin == -1 ? exporder[i] : exporder[i] - pmin - 1;
        offsets[4*i+1] = pmax == -1 ? patternSize - 1 - exporder[i] : pmax - 1 - exporder[i];
        offsets[4*i+2] = qmin == -1 ? pattern[exporder[i]]: pattern[exporder[i]] - pattern[qmin] - 1;
        offsets[4*i+3] = qmax == -1 ? patternSize - 1 - pattern[exporder[i]] : pattern[qmax] - 1 - pattern[exporder[i]];
    }

    // e[k] is true if we have already seen kth element
    bool e[patternSize];
    for(uint i = 0; i < patternSize; i++) e[i] = false;

    // now calculate back from last stage important elements for all stages
    for(int i = patternSize - 1; i >= 0; i--)
    {
        // add the next important elms
        for(int j = 0; j < 4; j++)
            if(impElms[4*i + j] != -1)
                e[impElms[4*i + j]] = true;

        // vector of unforgettable elements for ith stage
        std::vector<ushort> ue;

        // current index to unforgettable elms
        int ind = -1;

        // changed[k] is true if impElms[4*i + k] was updated to index
        bool changed[4] = {false, false, false, false};

        // create vector of the significant elms
        // and update impElms to indexes
        for(int j = 0; j < i; j++)
        {
            if(!e[exporder[j]]) continue;

            ue.push_back(exporder[j]);
            ind++;

            // change impElms to represent indexes to unfrgtElms[i]
            for(int k = 0; k < 4; k++)
            {
                if(!changed[k] && exporder[j] == impElms[4*i + k])
                {
                    impElms[4*i + k] = ind;
                    changed[k] = true;
                }

            }

        }

        // and move
        unfrgtElms[i] = std::move(ue);
    }

}

// tests current permutation for occurence of pattern
// returns true if pattern is embedded, false otherwise
bool Generator::test()
{
    // hash table for partial mappings
    map_table table;

    // add empty row
    table.emplace();

    // one step of algorithm
    for(uint i = 0; i < patternSize; i++)
    {
        // table of new partial mappings
        map_table ntable;

        // expand old mappings
        doOneStage(i, table, ntable);

        // no new partial mapping means no mappin of pattern
        if(ntable.size() == 0) return false;

        // and move
        table = std::move(ntable);
    }

    return true;
}

// tests current permutation for occurence of pattern with respect
// to position of the last moved element
// returns true if pattern is embedded, false otherwise
bool Generator::test(int elmX, int elmY)
{
    // table to hold partial mappings without moved element
    map_table table;

    // table with partial mappings containing moved element
    map_table elmTable;

    // add empty row
    table.emplace();

    // one step of algorithm
    for(uint i = 0; i < patternSize; i++)
    {

        // reference to unfrgtElms for speedup and better reading
        const auto& nextUE = unfrgtElms[i+1];
        const auto& thisUE = unfrgtElms[i];

        // references for better reading
        const auto& pmin = impElms[4*i];
        const auto& pmax = impElms[4*i+1];
        const auto& qmin = impElms[4*i+2];
        const auto& qmax = impElms[4*i+3];

        const auto& off1 = offsets[4*i];
        const auto& off2 = offsets[4*i+1];
        const auto& off3 = offsets[4*i+2];
        const auto& off4 = offsets[4*i+3];

        // table of new partial mappings without moved element
        map_table ntable;

        // table of new partial mappings with moved element
        map_table nelmTable;

        // for every partial mapping in old table
        for(auto it = table.begin(); it != table.end(); ++it)
        {
            // set coordinates of rectangle corresponding to partial mapping it
            int left = pmin == -1 ? 0 : (*it)[pmin] + 1;
            left += off1;

            int right = pmax == -1 ? permSize - 1 : (*it)[pmax] - 1;
            right -= off2;

            int bottom = qmin == -1 ? 0 : actPerm[(*it)[qmin]] + 1;
            bottom += off3;

            int top = qmax == -1 ? permSize - 1 : actPerm[(*it)[qmax]] - 1;
            top -= off4;

            // if moved element is inside the rectangle use it to extend partial mapping
            if(elmX <= right && elmX >= left && elmY >= bottom && elmY <= top)
                nelmTable.insert(extendMapping(thisUE, nextUE, (*it), elmX));

            // for each quadrant that needs to be checked, intersection with
            // rectangle is calculated and only that area gets checked
            if(quadrants[4*i])
            {
                int ntop = min(top, elmY - 1);
                int nleft = max(left, elmX + 1);

                if(ntop >= bottom && nleft <= right)
                    goThroughRect(thisUE, nextUE, off1, off2, off3, off4, nleft, right, ntop, bottom, (*it), ntable);
            }
            if(quadrants[4*i + 1])
            {
                int ntop = min(top, elmY - 1);
                int nright = min(right, elmX - 1);

                if(ntop >= bottom && nright >= left)
                    goThroughRect(thisUE, nextUE, off1, off2, off3, off4, left, nright, ntop, bottom, (*it), ntable);
            }
            if(quadrants[4*i + 2])
            {
                int nbottom = max(bottom, elmY + 1);
                int nleft = max(left, elmX + 1);

                if(nbottom <= top && nleft <= right)
                    goThroughRect(thisUE, nextUE, off1, off2, off3, off4, nleft, right, top, nbottom, (*it), ntable);
            }
            if(quadrants[4*i + 3])
            {
                int nbottom = max(bottom, elmY + 1);
                int nright = min(right, elmX - 1);

                if(nbottom <= top && nright >= left)
                   goThroughRect(thisUE, nextUE, off1, off2, off3, off4, left, nright, top, nbottom, (*it), ntable);
            }



        }

        // use normal extension for partial mappings containing moved element
        doOneStage(i, elmTable, nelmTable);

        // test if any valid mapping exists
        if(ntable.size() == 0 && nelmTable.size() == 0) return false;

        // and move
        table = std::move(ntable);
        elmTable = std::move(nelmTable);
    }

    return true;
}

// extend table to new one in stage number i
void Generator::doOneStage(uint i, const map_table& table, map_table& ntable)
{
    // reference to unfrgtElms for speedup and better reading
    const auto& nextUE = unfrgtElms[i+1];
    const auto& thisUE = unfrgtElms[i];

    // references for better reading
    const auto& pmin = impElms[4*i];
    const auto& pmax = impElms[4*i+1];
    const auto& qmin = impElms[4*i+2];
    const auto& qmax = impElms[4*i+3];

    const auto& off1 = offsets[4*i];
    const auto& off2 = offsets[4*i+1];
    const auto& off3 = offsets[4*i+2];
    const auto& off4 = offsets[4*i+3];

    for(auto it = table.begin(); it != table.end(); ++it)
    {
        // set coordinates of rectangle corresponding to partial mapping it
        int left = pmin == -1 ? 0 : (*it)[pmin] + 1;
        left += off1;

        int right = pmax == -1 ? permSize - 1 : (*it)[pmax] - 1;
        right -= off2;

        int bottom = qmin == -1 ? 0 : actPerm[(*it)[qmin]] + 1;
        bottom += off3;

        int top = qmax == -1 ? permSize - 1 : actPerm[(*it)[qmax]] - 1;
        top -= off4;

        // fill in the rectangle
        goThroughRect(thisUE, nextUE, off1, off2, off3, off4, left, right, top, bottom, (*it), ntable);
    }

}

// expands partial mapping with elements in given rectangle, with respect to offsets
void Generator::goThroughRect(const perm& thisUE, const perm& nextUE,
    const short& off1, const short& off2, const short& off3, const short& off4,
    int left, int right, int top, int bottom, const perm& it, map_table& ntable)
{
    // boundaries are tight from all 4 sides
    // or tight boundaries from left, bottom and top - looking for first element
    if(off1 == 0 && off3 == 0 && off4 == 0)
    {
        for(int j = left; j <= right; j++)
        {
            // test extension
            if(actPerm[j] < bottom || actPerm[j] > top) continue;

            ntable.insert(extendMapping(thisUE, nextUE, it, j));
            break;
        }

        if(nextUE.size() == 0 && ntable.size() > 0) return;
    }
    // tight boundaries from right, bottom and top - looking for first element from right
    else if(off2 == 0 && off3 == 0 && off4 == 0)
    {
        for(int j = right; j >= left; j--)
        {
            // test extension
            if(actPerm[j] < bottom || actPerm[j] > top) continue;

            ntable.insert(extendMapping(thisUE, nextUE, it, j));
            break;
        }
    }
    // tight boundaries from left, right and bottom - looking for minimum
    else if(off1 == 0 && off2 == 0 && off3 == 0)
    {
        // variable for best found element
        int minimum = -1;

        for(int j = left; j <= right; j++)
        {
            // test extension
            if(actPerm[j] < bottom || actPerm[j] > top) continue;

            if(minimum == -1 || actPerm[j] < actPerm[minimum]) minimum = j;
        }

        if(minimum != -1) ntable.insert(extendMapping(thisUE, nextUE, it, minimum));
    }
    // tight boundaries from left, right and top - looking for maximum
    else if(off1 == 0 && off2 == 0 && off4 == 0)
    {
        // variable for best found element
        int maximum = -1;

        for(int j = left; j <= right; j++)
        {
            // test extension
            if(actPerm[j] < bottom || actPerm[j] > top) continue;

            if(maximum == -1 || actPerm[j] > actPerm[maximum]) maximum = j;
        }

        if(maximum != -1) ntable.insert(extendMapping(thisUE, nextUE, it, maximum));
    }
    // left and bottom tight boundaries - looking for left minimums
    else if(off1 == 0 && off3 == 0)
    {
        // variable for best found element
        int minimum = -1;

        for(int j = left; j <= right; j++)
        {
            // test extension
            if(actPerm[j] < bottom || actPerm[j] > top) continue;

            if(minimum == -1 || actPerm[j] < actPerm[minimum])
            {
                minimum = j;
                ntable.insert(extendMapping(thisUE, nextUE, it, minimum));
            }
        }
    }
    // left and top tight boundaries - looking for left maximums
    else if(off1 == 0 && off4 == 0)
    {
        // variable for best found element
        int maximum = -1;

        for(int j = left; j <= right; j++)
        {
            // test extension
            if(actPerm[j] < bottom || actPerm[j] > top) continue;

            if(maximum == -1 || actPerm[j] > actPerm[maximum])
            {
                maximum = j;
                ntable.insert(extendMapping(thisUE, nextUE, it, maximum));
            }
        }
    }
    // right and top tight boundaries - looking for right maximums
    else if(off2 == 0 && off4 == 0)
    {
        // variable for best found element
        int maximum = -1;

        for(int j = right; j >= left; j--)
        {
            // test extension
            if(actPerm[j] < bottom || actPerm[j] > top) continue;

            if(maximum == -1 || actPerm[j] > actPerm[maximum])
            {
                maximum = j;
                ntable.insert(extendMapping(thisUE, nextUE, it, maximum));
            }
        }
    }
    // right and bottom tight boundaries - looking for right minimums
    else if(off2 == 0 && off3 == 0)
    {
        // variable for best found element
        int minimum = -1;

        for(int j = right; j >= left; j--)
        {
            // test extension
            if(actPerm[j] < bottom || actPerm[j] > top) continue;

            if(minimum == -1 || actPerm[j] < actPerm[minimum])
            {
                minimum = j;
                ntable.insert(extendMapping(thisUE, nextUE, it, minimum));
            }
        }
    }
    // boundaries are not special
    else
    {
        for(int j = left; j <= right; j++)
        {
            // test extension
            if(actPerm[j] < bottom || actPerm[j] > top) continue;

            // insert extended mapping
            ntable.insert(extendMapping(thisUE, nextUE, it, j));
        }

    }
}

// extends old mapping to new by adding new element
// returns new partial mapping
perm Generator::extendMapping(const perm& thisUE, const perm& nextUE, const perm& mapping, uint j)
{
    // create new row and copy values
    perm nrow(nextUE.size());

    // copy values
    uint l = 0;

    // copy the next unforgettable elements
    for(int k = 0; k < (int) nextUE.size() - 1; k++)
    {
        while(thisUE[l] != nextUE[k]) l++;
        nrow[k] = mapping[l];
    }

    // last element
    if(nextUE.size() != 0)
    {
        while(l < thisUE.size() && thisUE[l] != nextUE[nextUE.size() - 1]) l++;
        if(l == thisUE.size()) nrow[nextUE.size() - 1] = j;
        else nrow[nextUE.size() - 1] = mapping[l];
    }

    return nrow;
}

// moves element at position i to position j
void Generator::moveElm(uint i, uint j)
{
    ushort c = actPerm[i];
    if(i == j) return;
    else if(i < j)
    {
        std::copy(actPerm.begin() + i + 1, actPerm.begin() + j + 1, actPerm.begin()  + i);
        actPerm[j] = c;
    }
    else
    {
        std::copy_backward(actPerm.begin()  + j, actPerm.begin() + i, actPerm.begin() + i + 1);
        actPerm[j] = c;
    }
}

// struct which hold information when looking for the best exposure order
struct OrderValue
{
    // number of maximum unforgettable elements during one stage
    ushort maxUE;
    // number of stages with maxUE unforgettable elements
    ushort numOfMaxUE;

    OrderValue(ushort m, ushort n) : maxUE(m), numOfMaxUE(n) {}
};

// and operator which comes at hand when comparing two values
bool operator<(const OrderValue& x, const OrderValue& y)
{
    // lexicographic comparison
    return std::tie(x.maxUE, x.numOfMaxUE) < std::tie(y.maxUE, y.numOfMaxUE);
}

// finds best possible exposure order, works in O(2^patternSize) time
// uses finding shortest path in DAG
void Generator::findExpOrder()
{
    // working queue
    // subsets coded as binary numbers
    deque<uint> queue;
    queue.push_back(0);

    // precalculate 2^patternSize
    uint size = 1 << patternSize;

    // array of distances
    vector<OrderValue> distances(size, OrderValue(-1,0));
    distances[0].maxUE = 0;
    distances[0].numOfMaxUE = 1;

    // array with back traces to get the best exposure order
    vector<ushort> backTrace(size, -1);

    while(queue.size() > 0)
    {
        // get the front subset
        uint code = queue.front();
        queue.pop_front();

        // next value
        ushort thisUE = numOfUnfrgtElms(code);
        ushort thisNumOfUE = thisUE == distances[code].maxUE ?
                            distances[code].numOfMaxUE + 1 : 1 ;
        OrderValue value = max(OrderValue(thisUE, thisNumOfUE), distances[code]);

        // try to move to next subsets
        for(uint i = 0; i < patternSize; ++i)
        {
            // continue if this element already is in permutation
            if((code >> i) & 1)
                continue;

            // code for the new subset
            uint newCode = code | (1 << i);

            // if this is improvement
            if(value < distances[newCode])
            {
                // test if this element is not yet in queue
                if(backTrace[newCode] == (ushort) -1)
                    queue.push_back(newCode);

                // and update the values
                distances[newCode] = value;
                backTrace[newCode] = i;
            }
        }

    }

    // reading exposure order in reverse order jumping through back traces
    perm revExpOrder(patternSize);
    uint position = size - 1;

    for(uint i = 0; i < patternSize; ++i)
    {
        revExpOrder[i] = backTrace[position];
        position = position - (1 << backTrace[position]);
    }

    // reverse and move - set the exporder
    reverse(revExpOrder.begin(), revExpOrder.end());
    exporder = std::move(revExpOrder);
}

// return number of unforgettable elms for subset of indexes {0..patternSize-1}
// the subset is represented as vector of bools
ushort Generator::numOfUnfrgtElms(const uint code)
{
    ushort num = 0;

    // vector of flags whether index is in subset
    vector<bool> elms(patternSize + 2, false);

    // vector of flags whether pattern value is in subset
    vector<bool> elmsPat(patternSize + 2, false);

    // set boundaries
    elms[0] = true;
    elms[patternSize + 1] = true;
    elmsPat[0] = true;
    elmsPat[patternSize + 1] = true;

    // set flags from input subset
    for(uint i = 0; i < patternSize; ++i)
    {
        if(!((code >> i) & 1)) continue;
        elms[i + 1] = true;
        elmsPat[pattern[i] + 1] = true;
    }

    // calculate number of unforettable elements
    for(uint i = 0; i < patternSize; ++i)
    {
        if(!((code >> i) & 1)) continue;
        if(!elms[i] || !elms[i + 2] ||
            !elmsPat[pattern[i]] || !elmsPat[pattern[i] + 2])
            num++;
    }

    return num;
}

// function calculates possible quadrants in relation to the moved element
// when trying to find pattern in give exposition order
void Generator::calcQuadrants()
{
    // for each stage
    for(ushort i = 0; i < patternSize; ++i)
    {
        // current element coordinates
        ushort elmX = exporder[i];
        ushort elmY = pattern[elmX];

        for(ushort j = i + 1; j < patternSize; ++j)
        {
            // this element coordinates
            ushort thisX = exporder[j];
            ushort thisY = pattern[thisX];

            // set the corresponding quadrant
            if(thisX < elmX && thisY > elmY) quadrants[4*i]     = true;
            if(thisX > elmX && thisY > elmY) quadrants[4*i + 1] = true;
            if(thisX < elmX && thisY < elmY) quadrants[4*i + 2] = true;
            if(thisX > elmX && thisY < elmY) quadrants[4*i + 3] = true;
        }
    }
}
