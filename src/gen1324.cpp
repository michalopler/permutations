#include <fstream>
#include <algorithm>
#include <assert.h>

#include "gen1324.hpp"

using namespace std;

// only argument is the length of permutation
Generator1324::Generator1324(uint ps):
    permSize(ps), markovPos(0), moveCount(0),
     rd(), re(rd()), rg(0, ps-1)
{
    boundLeft = new uint[permSize];
    boundRight = new uint[permSize];

    actPerm = new ushort[permSize];

    for(uint i = 0; i < permSize; ++i)
    {
        boundLeft[i] = 0;
        boundRight[i] = 0;
    }

    // default starting permutation is decreasing one
    for(uint i = permSize; i != 0; --i)
        actPerm[permSize - i] = i;

    calcLBounds(0, permSize);
    calcRBounds(0, permSize);
}

Generator1324::~Generator1324()
{
    delete[] boundLeft;
    delete[] boundRight;
    delete[] actPerm;
}

// sets starting permutation
void Generator1324::setInitialPerm(const perm& p)
{
    copy(p.begin(), p.end(), actPerm);

    calcLBounds(0, permSize);
    calcRBounds(0, permSize);
}


// move forward in Markov chain
void Generator1324::move(uint steps)
{
    for(uint n = 0; n < steps; ++n)
    {
        markovPos++;

        // get two random positions
        ushort a = rg(re);
		ushort b = rg(re);

        if(a == b) continue;

        moveElm(a,b);


        // only necessary parts
        calcLBounds(min(a,b), max(a,b) + 1);
        calcRBounds(min(a,b), max(a,b) + 1);

        assert(slowTest() == fastTest(a,b));

        // rollback when not succesful
        if(fastTest(a,b))
        {
            moveElm(b,a);
            calcLBounds(min(a,b), max(a,b) + 1);
            calcRBounds(min(a,b), max(a,b) + 1);
        }
        else moveCount++;
    }
    assert(!stupidTest());
}

// n^2 version of testing - working, but slow
bool Generator1324::slowTest()
{
    for(uint i = 0; i < permSize - 2; i++)
    {

        if(boundRight[i] == actPerm[i] || boundLeft[i] == actPerm[i]) continue;
        for(size_t j = i+1; j < permSize; j++)
        {
            if(boundRight[j] > actPerm[i] &&
                    actPerm[i] > actPerm[j] &&
                    actPerm[j] > boundLeft[i])
                return true;
        }
    }
    return false;
}

// linear test for 1324 pattern
bool Generator1324::fastTest(uint a, uint b)
{
    if(boundLeft[b] == actPerm[b])
    {
        // if we moved element to right, then there is no new pattern
        if(a <= b) return false;

        // move right and count left minimum
        int min = b + 1;

        // TODO for range
        for(uint i = b + 1; i < permSize; ++i)
        {
            // if we get under b, then skip
            if(actPerm[i] < actPerm[b]) continue;

            // found new minimum
            if(actPerm[i] < actPerm[min])
            {
                // test pattern
                if(boundRight[i] > actPerm[min]) return true;
                min = i;
            }
        }
    }
    else if(boundRight[b] == actPerm[b])
    {
        // if we moved element to left, then there is no new pattern
        if(a >= b) return false;

        // move left and count right maximum
        int max = b - 1;

        // TODO for range
        for(int i = b - 1; i >= 0; --i)
        {
            // if we get over b, then skip
            if(actPerm[i] > actPerm[b]) continue;

            // found new maximum
            if(actPerm[i] > actPerm[max])
            {
                // test pattern
                if(boundLeft[i] < actPerm[max]) return true;
                max = i;
            }
        }
    }
    else
    {
        // element is neither left nor right bound

        // end of interval depends whether a < b or the other way round
        int e = a < b ? a : 0;

        // try to place b as "2" in pattern
        for(int i = b; i >= e; --i)
        {
            // end if the left minimum is too high
            if(boundLeft[i] > actPerm[b]) break;

            // test for pattern
            if(actPerm[i] > actPerm[b] && actPerm[i] < boundRight[b])
                return true;
        }

        // end of interval depends whether a < b or the other way round
        e = a < b ? permSize - 1 : a;

        // try to place b as "3" in pattern
        for(int i = b; i <= e; ++i)
        {
            // end if the right maximum is too low
            if(boundRight[i] < actPerm[b]) break;

            // test for pattern
            if(actPerm[i] < actPerm[b] && actPerm[i] > boundLeft[b])
                return true;
        }
    }

    return false;
}

// stupid test
bool Generator1324::stupidTest()
{

    for(uint i = 0; i < permSize; ++i)
        for(uint j = i+1; j < permSize; ++j)
        {
            if(actPerm[i] > actPerm[j]) continue;
            for(uint k = j+1; k < permSize; ++k)
            {
                if(actPerm[k] > actPerm[j] || actPerm[k] < actPerm[i]) continue;
                for(uint l = k+1; l < permSize; ++l)
                    if(actPerm[i] < actPerm[k] && actPerm[k] < actPerm[j] && actPerm[j] < actPerm[l]) return true;
            }
        }
    return false;

}

// calculate left bounds - smallest elements from beginning
void Generator1324::calcLBounds(uint a, uint b)
{
    ushort min = a == 0 ? permSize + 5 : boundLeft[a-1];
    for(size_t i = a; i < b; ++i)
    {
        if(actPerm[i] < min) min = actPerm[i];
        boundLeft[i] = min;
    }
}

// calculate right bounds - largest elements from end
void Generator1324::calcRBounds(int a, uint b)
{
    ushort max = b == permSize ? 0 : boundRight[b];
    for(int i = b - 1; i >= a; --i)
    {
        if(actPerm[i] > max) max = actPerm[i];
        boundRight[i] = max;
    }
}

// moves element at position i to position j
void Generator1324::moveElm(uint i, uint j)
{
    ushort c = actPerm[i];
    if(i == j) return;
    else if(i < j)
    {
        std::copy(actPerm + i + 1, actPerm + j + 1, actPerm  + i);
        actPerm[j] = c;
    }
    else
    {
        std::copy_backward(actPerm  + j, actPerm + i, actPerm + i + 1);
        actPerm[j] = c;
    }
}
