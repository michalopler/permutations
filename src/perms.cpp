#include <algorithm>
#include "perms.hpp"

using namespace std;

// helper function that prints permutation to output stream
void writeOutPerm(const perm& p, ostream& outStream) {
    for(uint i = 0; i < p.size(); i++) outStream << p[i] << " ";
    outStream << endl;
}

// helper function, which saves data to file for gnuplot
void savePlotData(const perm& p,const string& fileName) {
    ofstream outFile;
    outFile.open(fileName);

    if(!outFile.is_open()) return;

    for(uint i = 0; i < p.size(); i++)
        outFile << i+1 << " " << p[i] << endl;

    outFile.close();
}

// normalize permutation to contatin elms of range {0..p.size()-1}
perm normalizePerm(const perm &p)
{
    perm sortedPerm = p;
    sort(sortedPerm.begin(), sortedPerm.end());
    perm newPerm(p.size());
    for(uint i = 0; i < p.size(); ++i)
        newPerm[i] = find(sortedPerm.begin(), sortedPerm.end(), p[i]) - sortedPerm.begin();
    return newPerm;
}
