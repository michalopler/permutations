#include <fstream>
#include <algorithm>

#include "genincreasing.hpp"

using namespace std;

// takes two arguments - pattern length and permutation length
GeneratorIncreasing::GeneratorIncreasing(uint p, uint ps):
    patternSize(p), permSize(ps), markovPos(0), moveCount(0),
    incData(NULL), decData(NULL), rd(), re(rd()), rg(0, ps-1)
{
    // prepare data for caching
    incData = new ushort[(patternSize + 2)*permSize]();
    decData = new ushort[(patternSize + 2)*permSize]();

    // default starting permutation is decreasing one
    for(uint i = permSize; i != 0; --i)
        actPerm.push_back(i);

    setInitialPerm(actPerm);
}

GeneratorIncreasing::~GeneratorIncreasing()
{
    delete[] incData;
    delete[] decData;
}

// sets starting permutation
void GeneratorIncreasing::setInitialPerm(const perm& p)
{
    actPerm = p;

    // this is a little trick which allows me to use one function to calculate
    // the largest increasing and decreasing subsequences in one function
    actPermR = actPerm;
    reverse(actPermR.begin(), actPermR.end());
    for_each(actPermR.begin(), actPermR.end(), [](ushort& p) {p = -p;});

    // prepare data for computing
    fillIncData(0);
    fillDecData(permSize-1);
}


// move forward in Markov chain
void GeneratorIncreasing::move(uint steps)
{
    for(uint i = 0; i < steps; ++i)
    {
        // get two random positions
        ushort a = rg(re);
		ushort b = rg(re);

        // use cached data to calculate the largest increasing subsequence containing element at position a
        ushort subPattern;
        if(a == b) continue;
        else if(b > a) subPattern = getLongestIncPrefix(b + 1,actPerm[a]) + getLongestDecSufix(b + 1,actPerm[a]) - 1;
        else subPattern = getLongestIncPrefix(b,actPerm[a]) + getLongestDecSufix(b,actPerm[a]) - 1;


        if(subPattern < patternSize)
        {
            // move element in permutation
            moveElm(a,b,actPerm);

            // and in its reverse
            moveElm(permSize - 1 - a, permSize - 1 - b, actPermR);

            // recalculate the changed parts of the permutation
            fillIncData(min(a,b));
            fillDecData(max(a,b));

            moveCount++;
        }
    }
}

// moves element at position i to position j
void GeneratorIncreasing::moveElm(uint i, uint j, perm& p)
{
    ushort c = p[i];
    if(i == j) return;
    else if(i < j)
    {
        std::copy(p.begin() + i + 1, p.begin() + j + 1, p.begin() + i);
        p[j] = c;
    }
    else
    {
        std::copy_backward(p.begin() + j, p.begin() + i, p.begin() + i + 1);
        p[j] = c;
    }
}

// recalculates data for longest ascending pattern from index i
// to end of the permutation
uint GeneratorIncreasing::fillIncData(uint i)
{
    return fillData(i, actPerm, incData);
}

// recalculates data for longest descending pattern backwards from
// index i to the beginning
uint GeneratorIncreasing::fillDecData(uint i)
{
    return fillData(permSize - i - 1, actPermR, decData);
}

// recalculates data for longest pattern
uint GeneratorIncreasing::fillData(uint i, const perm& p, ushort* m)
{
    uint l = 0;
    if(i != 0) l = m[i*(patternSize + 2) - 1];


    for(;i < permSize; ++i) {
        // copy last column of data
        if(i != 0)
            for(unsigned j = 0; j < patternSize + 2; j++)
                m[i*(patternSize + 2) + j] = m[(i - 1)*(patternSize + 2) + j];

        uint j = 0;
        while(j < l && p[m[i*(patternSize + 2) + j+1]] < p[i]) j++;
        if(p[m[i*(patternSize + 2) + j]] >= p[i]) j = 0;
        if(j == l || p[i] < p[m[i*(patternSize + 2) + j+1]]) {
                m[i*(patternSize + 2) + j+1] = i;
                l = max(l, j+1);

                // store actual value of l in last value
                m[(i+1)*(patternSize + 2) - 1] = l;

                // we are not interested in longer patterns
                if(l == patternSize) break;
        }
    }
    return l;
}


// tries to place element e at index i and returns length of longest
// increasing subsequence ending in e
uint GeneratorIncreasing::getLongestIncPrefix(uint i, ushort e)
{
    return longestIncPrefix(i, e, actPerm, incData);
}

// tries to place element e at index i and returns length of longest
// decreasing subsequence in reversed permutation ending in e
uint GeneratorIncreasing::getLongestDecSufix(uint i, ushort e)
{
    return longestIncPrefix(permSize - i, -e, actPermR, decData);
}

// uses cache data m obtained during initial calculations to get longest
// subsequence in permutation perm, ending on element e at index i
uint GeneratorIncreasing::longestIncPrefix(uint i, ushort e, const perm& p, ushort* m)
{
    if(i == 0) return 1;

    uint j = 0;
    uint offset = (i-1)*(patternSize + 2);
    while(j < m[offset + patternSize + 1] && p[m[offset + j+1]] < e) j++;
    if(p[m[offset + j]] >= e) j = 0;

    return j+1;
}
