\documentclass[11pt]{article}
\usepackage[utf8x]{inputenc}
\usepackage[english]{babel}
\usepackage{a4wide}
\usepackage{listings}
\usepackage{amsmath,amsfonts,amssymb}
\usepackage[hidelinks]{hyperref}
\usepackage{xcolor}
\usepackage{textcomp}

\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  xleftmargin=\parindent,
  language=C++,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{green!40!black},
  commentstyle=\color{purple!40!black},
  identifierstyle=\color{blue},
  stringstyle=\color{orange},
}

\lstset{style=customc}

\title{\bfseries {\Huge Permutation Generator} \\ Documentation}
\date{}

\begin{document}
\maketitle
\tableofcontents 
\newpage

\section{Introduction}
Main objective of this library is to generate permutations of fixed length that do not contain given (fixed) pattern. 
It uses the {\em Markov chain Monte Carlo} (MCMC) method, which is thoroughly described in article by N. Madras and Hailong Liu \cite{rpap}.

There are in fact three different generators sharing the same basic algorithm. 
Two of them are aimed at special classes of patterns - monotonically increasing patterns of any length and pattern 1324. 
The third one works for any pattern (although it is obviously much slower in these special cases).

\section{Basic algorithm}
Informal description of the basic algorithm follows (proper formal description can be found in \cite{rpap}):

The generator constructs a discrete-time Markov chain whose state space is the class of permutations avoiding given pattern. 
The Markov chain starts with a simple permutation avoiding the pattern (in our case monotonically increasing or decreasing). 
Then each step in Markov chain consists of moving one randomly selected element in the current permutation to new place. 
If the new permutation contains the pattern we set the old one as next in our chain. 
Otherwise the newly created one is set. 
As we move along this Markov chain the permutations converge to random permutation in uniform distribution (again see \cite{rpap}).

The crucial part of this generator lies in the searching for the given pattern because this problem is in general (for variable length of pattern) NP-complete.

\section{Pattern searching}

There is one important observation used in all searching algorithms.
If any permutation does not contain given pattern but the permutation created by moving one element does then the mapping of such pattern must use the moved element.
Informal description of the three algorithms follows.

\subsection{Monotonically increasing patterns}

\paragraph{}

Basic idea behind testing for occurrences of monotonically increasing pattern is to use the known algorithm for finding largest increasing subsequence and simply test if it is shorter than pattern length. 
Description of this basic algorithm follows (it's shorter version of the description on Wikipedia \cite{wikilis}):

Denote the sequence values as $X[0]$, $X[1]$, etc. 
Then, after processing $X[i]$, the algorithm will have stored values in an array $M$, that $M[j]$ stores the index $k$ of the smallest value $X[k]$ such that there is an increasing subsequence of length $j$ ending at $X[k]$ on the range $k \leq i$ .
In addition the algorithm stores a variable $L$ representing the length of the longest increasing subsequence found so far. 

For each $i$ then we search for the largest $j \leq L$ such that $X[M[j]] \leq X[i]$, i.e. the longest increasing subsequence that can be extended with $X[i]$. 
If $j$ is equal to $L$ we increase $L$ by one and set $M[j+1] = i$. 
On top of that if we find a subsequence ending on smaller number ($X[i] < X[j+1]$) we update $M[j+1] = i$.

Let us denote pattern length as $k$ and permutation length as $n$. 
Notice that since we are looking for subsequence of length $k$, it is sufficient for array $M$ to have size $k + 1$. 
Then clearly this algorithm has time complexity $O(nk)$. 
It could be implemented with binary search to improve to $O(n \cdot \log k)$, however it would not be an improvement for small $k$ which is our typical case.

\paragraph{}

The generator uses modification of this algorithm in order to use the fact that only one element has been moved since the last search. 
The modification lies in remembering the history of this computation - that is we replace array $M$ with two dimensional array $N[k][j]$, where $N[k]$ is basically the state of array $M$ before processing $X[k]$. 
That means that $k$-th step of the algorithm (for $k \geq 1$) begins with copying array $M[k-1]$ to $M[k]$.

Now suppose we moved element from position $a$ to $b$. 
We can then quickly compute length $L_1$ of the longest increasing subsequence in the prefix ending on the moved element by simply using the precomputed array $N[b-1]$ and the moved element in one step of the algorithm described above. 
This operation takes only $O(k)$ time.

Another simple trick allows us to compute the longest increasing subsequence in the whole permutation. 
The generator simply does the same computation for the largest decreasing subsequence from the end. 
Therefore in the same way it obtains length $L_2$ of the longest increasing subsequence in the suffix beginning with the moved element. 

Length of the longest increasing subsequence in the whole permutation, which uses the moved element, is then simply $L = L1 + L2 - 1$. 
Therefore we are able to check if the new permutation contains the increasing pattern of length $k$ in time $O(k)$.

\paragraph{}

However if the move is successful we have to run the algorithm to fill the N array in the corresponding parts - that is in the suffix beginning on min($a$,$b$) for the increasing subsequence and in the prefix ending on max($a$,$b$) for the decreasing one.


\subsection{Pattern 1324}

\paragraph{}

{\em Left minimum} is an element in permutation wchich is smaller than all its predecessors. 
{\em Right minimum} is similairly an element that is smaller than all its successors.
{\em Left} and {\em right maximums} are defined in the same way.
Left minimum of element $x$ is the smallest element in the prefix ending on $x$ (symmetrically for the others).

\paragraph{}

The basic idea behind the testing algorithm is that whenever any permutation contains the pattern 1324 then it contains mapping of the pattern such that "1" is matched to some left minimum and "4" is matched to some right maximum. 
That is easy to prove because given any mapping of the pattern we can simply change the mapping of "1" to its left minimum and "4" to its right maximum.
That way we have clearly obtained a mapping of the pattern satisfying our condition.

The algorithm stores left minimums and right maximums in two arrays with the same length as the generated permutations.
$L[k]$ stores the value of the left minimum of the $k$-th element and $R[k]$ holds the value of its right maximum.
That way we can obtain the values of left minimums and maximums of any element in constant time.

\paragraph{}

Suppose we have moved element $x$ from position $a$ to $b$. The algorithm will do the following:

\paragraph{}

If $x$ became left minimum (and symmetrically for right maximum) then any mapping of the pattern must map "1" to $x$.
  
We will show that in this case if the permutation contains the pattern 1324 then it contains mapping of the pattern such that both "3" and "2" are mapped to elements, which are smaller than all the elements between them and $x$.
Now suppose we have some mapping of the pattern that does not have this property.
For the mappings of "3" and "2" we define their {\em candidates} as the smallest element between $x$ and the mapping of "3" (and "2") which is larger than $x$.
If the candidate of "3" is the same as candidate of "2" then there is an element preceding both mappings of "3" and "2" which is smaller than both of them. 
But that would mean that there was a mapping of the pattern before the movement of $x$. 
Therefore the candidates of "3" and "2" must be different.
And by changing the mapping of "3" and "2" to their candidates we obtain mapping which has the desired property.
  
\paragraph{}
  
The algorithm simply goes through all elements following $x$ and remembers the smallest element larger than $x$.
When it finds smaller element it tries to map "3" to the old minimum and "2" to the new minimum.
There is a mapping of the pattern if and only if the right maximum of the new minimum is larger than the old minimum.
Therefore in this case we can find the pattern in time $O(n)$.
  
\paragraph{}

If $x$ is not left minimum nor right maximum then we simply try to map it firstly as the "3".
The left minimum of $x$ then gets mapped as "1".
We try all possible elements as the mapping of "2" and their right maximums as the mapping of "4".
Then the algorithm similairly tries to map $x$ as the "2".
Therefore in this case we can also find the pattern in time $O(n)$.


In conlusion our algorithm can check if the new permutation contains the pattern 1324 in time $O(n)$.

\subsection{General approach}

Firstly a simple definition. 
{\em Neighbours} of an element in permutation are the elements right next to it and the elements larger and smaller by one.
E.g. in permutation 13254 are the neighbours of 2 the elements 3, 5 and 1.

\subsubsection{Basic algorithm}
The algorithm for finding any pattern is taken from an article by Shlomo Ahal and Yuri Rabinovich \cite{subpattern}.
It is basically a clever way to try all the possibilities of inclusion of the pattern.
The basic idea is that we try to map the pattern in some order (called {\em exposure order}).
After each step we remember all possible {\em partial mappings} of the pattern and then we try to extend them by mapping the next element in the exposure order.
Each partial mapping then limits the position of the next element with at most four elements (for details see  \cite{subpattern}).
In another way the element which gets mapped must lie in specific rectangle in the permutation graph (this approach will prove to be useful later).

\subsubsection{Offsets}

Now suppose we are trying to extend some partial mapping with next element.
We are limited to some rectangle in the permutation graph (as described above).
But the rectangle isn't typically defined by the neighbours of our element.
For example the left bound is created by existing mapping of "1" and we are trying to map "4".
Clearly "4" must be on the right of "1", but there will also be 2 elements between them.
Therefore we define {\em offsets} (left, right, bottom and top) as the number of elements between the current element and the one defining the corresponding side of the rectangle.

\paragraph{}

The improvement of the algorithm uses offsets in two ways.
The first one trivially shrinks the rectangle with respect to the offsets.
The second one is more interesting.
The basic idea is to simplify things when some of the sides of the rectangle are defined by the neighbours of the current element.
When the neighbours define all four sides of the rectangle we can use any element inside to extend the mapping.
When the neighbours define three sides of the rectangle we can use just one element to extend the mapping.
Depending on which three sides are defined by neighbours we use either first element, last element, minimum or maximum.
And lastly if two adjacent sides of the rectangle are defined by the neighbours we use the properties of left and right maximums and minimums as desribed in the algorithm for finding the patttern 1324.

\subsubsection{Quadrants}

Our improvements still doesn't use the fact that we know which element was moved and therefore must be part of the mapping.
When we tak a look at any element in the permutation graph then it naturally splits the graph into four {\em quadrants}.
The basic idea is that when we are extending partial mapping that does not contain our element we don't have to extend it in a way that this element can't get mapped.
E.g. there is no reason to map the "4" in 1324 to an element on the left of the moved one, because then it will never get mapped.
At the beginning we can compute for the elements in exposure order which of their quadrants contain elements that will be mapped after them.
Then during the mapping we keep separately the partial mappings that contain our element and those that doesn't.
Now suppose we are extending some partial mapping that does not contain it.
Then we can limit the rectangle with respect to the quadrants.

\subsubsection{Finding the exposure order}

Firstly we must look at what really makes exposure order better than others.
It is how much time we spend in the expanding of the mappings - that is how many unforgettable elements (see \cite{subpattern}) are needed in each stage and how many stages are faster by using the improvements concerning offsets.
The most straightforward way to find the best exposure order would be by brute force but that is not usable for longer patterns since it takes $O(k!)$ time.

\paragraph{}

A simple observation will let us to find it faster.
Suppose we already mapped a subset of the pattern then the complexity of the extension with next element does not depend on the order in which we mapped the subset.
Therefore we can translate our problem into finding the "best" path in directed oriented graph - Hasse diagram of the set of all subsets of our pattern.
This problem can be solved in time linear to the number of vertices - therefore in time $O(2^k)$, which is significant improvement.

\section{Usage}

Firstly there is a definition of {\em perm} data structure for permutations and few helper functions - for printing permutation out and saving it in format suitable for generating graphs with {\em gnuplot}.

The usage of the generator is fairly simple.
Just create your instance of one of the three classes. 
The constructors of the three classes are different - the one for increasing permutations takes the  pattern length and the permutation length, the 1324 one take just the length of the permutation and the general one takes the pattern and permutation length.
Then there are just a few other member functions:

\paragraph{\ttfamily void move(uint s)}
This function moves forward in the Markov chain by s steps.

\paragraph{\ttfamily perm getPerm()}
Returns the current permutation in the Markov chain.

\paragraph{\ttfamily uint getMoveCount()}
Returns the number of successful moves (the ones where pattern wasn't find).

\paragraph{\ttfamily void setSeed(uint value)}
Sets the seed of the random number generator to given value. 
Useful for testing and reproducing the same behaviour.

\subsection{Simple example}

\begin{lstlisting}
#include "genany.hpp"

int main()
{
// generator for permutations of length 100 avoiding pattern 13245 
Generator g({1,3,2,4,5}, 100);

// move 1 000 000 steps in the Markov chain
g.move(1000000);
 
// and print output
cout << g.getPerm();

return 0;
}
\end{lstlisting}


\newpage
\begin{thebibliography}{9}
\bibitem{rpap}N.Madras and Hailong Liu, {\em Random pattern-avoiding permutations,}
           Contemporary Mathematics, Volume 520, 2010
\bibitem{subpattern}Shlomo Ahal and Yuri Rabinovich, {\em On complexity of the subpattern problem,}
           SIAM J. Discrete Math., 22(2), 2008
\bibitem{wikilis}{\em Longest increasing subsequence,}
           Wikipedia,\\ \url{http://en.wikipedia.org/wiki/Longest\_increasing\_subsequence}
\end{thebibliography}

\end{document}