#ifndef GENANY_HPP
#define GENANY_HPP

#include <vector>
#include <unordered_set>
#include <set>
#include <random>
#include "perms.hpp"


class Generator
{
public:
    typedef std::set<perm> map_table;

    Generator(const perm& p, uint n);
    ~Generator();

    void move(uint);
    perm getPerm() {return actPerm;}
    uint getMoveCount() {return moveCount;}
    void setSeed(uint value) {re.seed(value);}

private:
    void calcImpElms();
    bool test();
    bool test(int, int);
    void moveElm(uint, uint);

    perm extendMapping(const perm&, const perm&, const perm&, uint);
    void doOneStage(uint, const map_table&, map_table&);
    void goThroughRect(const perm&, const perm&,
        const short&, const short&, const short&, const short&,
        int, int, int, int, const perm&, map_table& );

    void findExpOrder();
    void calcQuadrants();

    ushort numOfUnfrgtElms(const uint);

    // best expostition order
    perm exporder;

    // pattern we are avoiding
    const perm pattern;

    // current permutation in Markov chain
    perm actPerm;

    // vector which stores the four important elements for each stage
    // stores them as indexes to unfrgtElms
    // if some of the is not available, then contains -1 instead
    // 4k + 0,1,2,3 - pmin, pmax, qmin, qmax
    std::vector<short> impElms;

    // vector wchich stores the minimum offsets form important elms
    std::vector<short> offsets;

    // vector of unforgettable elements
    // kth element is vector of unforgettable elements in kth stage
    std::vector<perm> unfrgtElms;

    // vector with possible quadrants for placing element in each stage
    // used in improved testing considering the position of moved element
    // for each stage 4k + 0, 1, 2, 3 is upper left, upper right, bottom left
    // and bottom right
    std::vector<bool> quadrants;

    // position in Markov chain
    uint markovPos;

    // number of succesful moves in Markov chain
    uint moveCount;

    // sizes of permutation and pattern
    uint permSize;
    uint patternSize;

    // random objects
    std::random_device rd;
	std::default_random_engine re;
	std::uniform_int_distribution<int> rg;
};

#endif // GENANY_HPP
