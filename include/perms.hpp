#ifndef PERMS_HPP
#define PERMS_HPP

#include <vector>
#include <iostream>
#include <fstream>
#include <string>

typedef unsigned int uint;
typedef unsigned short ushort;

// probably own data structure later
// but fast enough for now
typedef std::vector<ushort> perm;


// helper functions,probably added to data structure later
void writeOutPerm(const perm&, std::ostream&);
void savePlotData(const perm&, const std::string&);
perm normalizePerm(const perm&);

// hashing function for permutations
class PermHash
{
public:
    std::size_t operator()(perm const& p) const
    {
        size_t seed = 0;
        for(auto it = p.begin(); it != p.end(); ++it)
        {
            seed ^= std::hash<ushort>()(*it) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        }
        return seed;
    }
};

#endif // PERMS_HPP
