#ifndef GENPERM1324_HPP
#define GENPERM1324_HPP

#include <vector>
#include <iostream>
#include <random>
#include "perms.hpp"


// randomly generating permutations
// without pattern 1324
class Generator1324
{
public:
    Generator1324(uint);
    ~Generator1324();

    void setInitialPerm(const perm&);
    void move(uint);
    perm getPerm() {return perm(actPerm, actPerm + permSize);}
    uint getMoveCount() {return moveCount;}
    void setSeed(uint value) {re.seed(value);}
    bool stupidTest();

private:
    void moveElm(uint, uint);
    void calcLBounds(uint, uint);
    void calcRBounds(int, uint);
    bool slowTest();
    bool fastTest(uint a, uint b);

    uint permSize;
    uint markovPos;
    uint moveCount;

    uint* boundLeft;
    uint* boundRight;

    // actual permutation and its reverse
    ushort* actPerm;

    // random objects
    std::random_device rd;
	std::default_random_engine re;
	std::uniform_int_distribution<int> rg;

};


#endif // GENPERM1324_HPP
