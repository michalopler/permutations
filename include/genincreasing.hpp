#ifndef GENPERM_HPP
#define GENPERM_HPP

#include <vector>
#include <iostream>
#include <random>
#include "perms.hpp"

// randomly generating permutations
// only those without increasing patterns
class GeneratorIncreasing
{
public:
    GeneratorIncreasing(uint, uint);
    ~GeneratorIncreasing();

    void setInitialPerm(const perm&);
    void move(uint);
    perm& getPerm() {return actPerm;}
    uint getMoveCount() {return moveCount;}
    void setSeed(uint value) {re.seed(value);}

private:
    uint fillIncData(uint);
    uint fillDecData(uint);
    uint fillData(uint, const perm&, ushort*);

    uint getLongestIncPrefix(uint, ushort);
    uint getLongestDecSufix(uint, ushort);
    uint longestIncPrefix(uint, ushort,const perm&, ushort*);
    
    void moveElm(uint, uint, perm&);

    uint patternSize;
    uint permSize;
    uint markovPos;
    uint moveCount;


    // actual permutation and its reverse
    perm actPerm;
    perm actPermR;

    // arrays to store cached results
    ushort* incData;
    ushort* decData;

    // random objects
    std::random_device rd;
	std::default_random_engine re;
	std::uniform_int_distribution<int> rg;

};


#endif // GENPERM_HPP
