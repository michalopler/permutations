CC     = g++
CFLAGS = -std=c++11 -Wall -Werror -Wextra
INCLUDES = -I./include
LFLAGS = -lstdc++

# Files
SRCS = $(wildcard src/*.cpp)
OBJS = $(notdir $(SRCS:.cpp=.o))
EXE  = test

# Debug
DBGDIR = debug
DBGEXE = $(DBGDIR)/$(EXE)
DBGOBJS = $(addprefix $(DBGDIR)/, $(OBJS))
DBGCFLAGS = -g -O0 -DDEBUG

# Release
RELDIR = release
RELEXE = $(RELDIR)/$(EXE)
RELOBJS = $(addprefix $(RELDIR)/, $(OBJS))
RELCFLAGS = -O3 -DNDEBUG

.PHONY: all clean debug prep release remake

# Default build
all: prep release

debug: $(DBGEXE)

$(DBGEXE): $(DBGOBJS)
	$(CC) $(CFLAGS) $(DBGCFLAGS) $(INCLUDES) -o $(DBGEXE) $^

$(DBGDIR)/%.o: src/%.cpp
	$(CC) -c $(CFLAGS) $(DBGCFLAGS) $(INCLUDES) -o $@ $<


release: $(RELEXE)

$(RELEXE): $(RELOBJS)
	$(CC) $(CFLAGS) $(RELCFLAGS) $(INCLUDES) -o $(RELEXE) $^

$(RELDIR)/%.o: src/%.cpp
	$(CC) -c $(CFLAGS) $(RELCFLAGS) $(INCLUDES) -o $@ $<


prep:
	@mkdir -p $(DBGDIR) $(RELDIR)

remake: clean all

clean:
	rm -f $(RELEXE) $(RELOBJS) $(DBGEXE) $(DBGOBJS)
